# trdump
An ingame time parser for Tomb Raider 1-3 PS1/PC savegames.

Tomb Raider 1 unfortunately only keeps track of the current level or the last level when saved at the save prompt at the end of a level (PS1)

Usage:

	trdump tr1-3_savegame
